package com.rmi.client.models;

import java.io.Serializable;

public class UserEtat implements Serializable {
	private String username;
	private int etat;
	
	public UserEtat() {
		super();

	}
	
	public UserEtat(String username,int etat) {
		this.username=username;
		this.etat=etat;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getEtat() {
		return etat;
	}
	public void setEtat(int etat) {
		this.etat = etat;
	}

}
