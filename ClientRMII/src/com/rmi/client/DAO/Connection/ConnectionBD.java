package com.rmi.client.DAO.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBD {
	private static Connection cnx;
	
	public static Connection getConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
	 try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			cnx=DriverManager.getConnection("jdbc:mysql://localhost:3306/rmibdd?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
			return cnx;
		}catch(SQLException e)
		{
			e.getMessage();
		}
		return null;
	}

}
