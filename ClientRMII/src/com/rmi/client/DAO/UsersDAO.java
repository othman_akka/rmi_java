package com.rmi.client.DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import com.rmi.client.models.User;

public interface UsersDAO {
	    boolean add(User user) throws SQLException;
	    User get(String username)throws SQLException;
}
