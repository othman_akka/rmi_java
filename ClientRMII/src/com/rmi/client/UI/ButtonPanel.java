package com.rmi.client.UI;

import java.awt.Cursor;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ButtonPanel extends JButton{
  public ButtonPanel(String icon) {
    setContentAreaFilled(false);
    setBorder(null);
    setCursor(new Cursor(Cursor.HAND_CURSOR));
    setIcon(new ImageIcon(icon));
    setName(icon);
  }
}
