package com.rmi.client.UI;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import com.rmi.client.BusnissClientImp.BusnissClient;
import com.rmi.client.DAO.Implement.UsersDAOImplement;
import com.rmi.client.models.UserEtat;
import com.rmi.server.Business.Business;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class ClientGUI extends JFrame {

	JFrame frame;
	private JTextField textField;
	public static JTextField textField_1= new JTextField();
	private JButton btnSend;
	private JButton button;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JLabel label;
	private JScrollPane scrollPane;
	private JPopupMenu popupmenu ;
	public static JTextPane textArea=new JTextPane();
	public JPopupMenu popup;
	public JPopupMenu popUpemji;
	private FormLogin form;
	UsersDAOImplement bdd;
	UserEtat userEtat;
	
	private JButton btnNewButton;
	static private boolean isShowingPopup = false;
	public static String urlEmoji;
	public static int index =0;
	public static int indexDisconnect =0;
	public static int emojii=0;
	public static int filee=0;
	public static int imagee=0;
	public JPanel panelImage ;
	JPanel panelg = new JPanel();
	JScrollPane scrolpane = new JScrollPane();
	Panel panel1 = new Panel();
	Panel panelemoji = new Panel();

	public ClientGUI()  {
		initialize();
		form=new FormLogin();
	}
	private void initialize() {
		frame = new JFrame("Client");
		frame.setBounds(800, 100, 450, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(new Color(0, 51, 102));
		panel.setBounds(10, 11, 414, 439);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(new Color(248, 148, 6));
		panel_1.setBounds(0, 0, 414, 38);
		panel.add(panel_1);
		
		textField = new JTextField();
		textField.setForeground(new Color(165, 42, 42));
		textField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField.setEnabled(false);
		textField.setColumns(10);
		textField.setText(form.getNameUser());
		textField.setBounds(98, 11, 109, 22);
		panel_1.add(textField);
		
	    button = new JButton("Connect");
		button.setForeground(Color.GRAY);
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button.setBackground(new Color(0, 230, 64));
		button.setBounds(295, 11, 109, 22);
		panel_1.add(button);
		
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
				if(button.getText().toString().equals("Connect"))
				{    
					button.setText("Disconnect");
					button.setBackground(new Color(236, 100, 75));
					try {
						String url = "rmi://localhost:1099/akka";
						Business server = (Business) Naming.lookup(url);
					    new Thread(new BusnissClient(FormLogin.nameUser, server)).start();
					} catch (MalformedURLException | RemoteException | NotBoundException e1) {
						e1.printStackTrace();
					}  
				       
				       
					
				}
				else
				{  ClientGUI.indexDisconnect =1;
					button.setText("Disconnect");
					button.setBackground(new Color(236, 100, 75));
					System.exit(0);
					
				}
			}
				
		});
		
		label = new JLabel("Your name :");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(10, 11, 78, 22);
		panel_1.add(label);
		
		panel_2 = new JPanel();
		panel_2.setBounds(10, 49, 258, 330);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		textArea.setBounds(3, 0, 255, 330);
		textArea.setEditable(false);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 258,330);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(textArea);;
		panel_2.add(scrollPane);
		
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField_1.setBounds(10, 402, 235, 26);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		btnSend = new JButton("SEND");
		btnSend.setForeground(new Color(255, 255, 255));
		btnSend.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSend.setBackground(new Color(255, 140, 0));
		btnSend.setBounds(315, 404, 89, 23);
		panel.add(btnSend);
		
        btnSend.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(button.getText().toString().equals("Disconnect") )
				{
					if(textField_1.getText().equals(""))
					{
						 JOptionPane d = new JOptionPane();
					     d.showMessageDialog( frame, "your Message's empty", "Message",JOptionPane.PLAIN_MESSAGE);
						
					}else
					{
					index=1;
					}
				}
				else
				{
					
					JOptionPane d = new JOptionPane();
				     d.showMessageDialog( frame, "Check from your connection", "Connection",JOptionPane.PLAIN_MESSAGE);
				}
			}
		});
	
		
		panelImage= new JPanel();
		panelImage.setMaximumSize(new Dimension(300, 200));
		panelImage.setMinimumSize(new Dimension(300, 200));
		
		Icon icon = new ImageIcon("C:\\Users\\pc\\eclipse\\ClientRMII\\icon\\4.png");
		btnNewButton = new JButton(icon);
		btnNewButton.setBounds(255, 402, 50, 26);
		btnNewButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ev) {
		    	popupmenu.show(btnNewButton,0,-90);
		    }
		});
		
		panel.add(btnNewButton);
		
		popupmenu = new JPopupMenu("list");
		popupmenu.setBounds(0,0, 45, 30);
		JMenuItem emoji = new JMenuItem(new ImageIcon("C:\\Users\\pc\\eclipse\\ClientRMII\\icon\\1.png"));  
		JMenuItem image = new JMenuItem(new ImageIcon("C:/Users/pc/eclipse/ClientRMII/icon/2.png"));  
		JMenuItem file = new JMenuItem(new ImageIcon("C:\\Users\\pc\\eclipse\\ClientRMII\\icon\\3.png"));  
		popupmenu.add(emoji); 
		popupmenu.add(image); 
		popupmenu.add(file);  
		
		popupmenu.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				 isShowingPopup = false;
			}
			
			@Override
			public void focusGained(FocusEvent e) {
			
				
			}
		});

        
        Button button = new Button("C:\\Users\\pc\\eclipse\\ClientRMII\\emotions\\5.png", 28);
        button.setName("emoji_yellow");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
               panelImage.removeAll();
                for (int i=1; i<=11; i++) {
                    ButtonPanel buttonPanel = new ButtonPanel("C:\\Users\\pc\\eclipse\\ClientRMII\\emotions\\"+i+".png");
                	buttonPanel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent ae) {
                          //  sendPic(buttonPanel.getName());
                        	 urlEmoji=buttonPanel.getName();
                            emojii=1;
                            
                      	  if (isShowingPopup) {
               			   isShowingPopup = false;
               			  } else {
               			   
               			   popUpemji.show(btnNewButton, -1,btnNewButton.getHeight());
               			   isShowingPopup = true;
               			  }
                           
                        }
                    });
                    panelImage.add(buttonPanel);
                }
                panelImage.revalidate();
                panelImage.repaint();
            }
        });
                    panelg.add(button);
                    sendPic(button);

        panelg.setBackground(new java.awt.Color(255, 255, 255));
        FlowLayout flowLayout2 = new FlowLayout(FlowLayout.LEFT, 0, 0);
        flowLayout2.setAlignOnBaseline(true);
        
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        panelg.setLayout(flowLayout2);
        scrollPane.setViewportView(panelg);
        
        panel1.setLayer(panelImage, JLayeredPane.DEFAULT_LAYER);
        panel1.setLayer(scrollPane,JLayeredPane.DEFAULT_LAYER);
        
       GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(Alignment.LEADING)
            .addGroup(Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(Alignment.TRAILING)
                    .addComponent(scrollPane)
                    .addComponent(panelImage,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        
        panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup(Alignment.LEADING)
                .addGroup(panel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(scrollPane,GroupLayout.PREFERRED_SIZE, 38,GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panelImage,GroupLayout.DEFAULT_SIZE,GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
            );
        panelemoji.setBackground(new Color(153, 153, 153));
        panelemoji.setMaximumSize(new Dimension(502, 349));
        panelemoji.setMinimumSize(new Dimension(502, 349));
        
        panelemoji.setLayer(panel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        GroupLayout panel_emojiLayout = new GroupLayout(panelemoji);
        panelemoji.setLayout(panel_emojiLayout);
        panel_emojiLayout.setHorizontalGroup(
            panel_emojiLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_emojiLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(panel1,GroupLayout.DEFAULT_SIZE,GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );
        panel_emojiLayout.setVerticalGroup(
            panel_emojiLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_emojiLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(panel1,GroupLayout.DEFAULT_SIZE,GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );
        
    	emoji.addActionListener(new ActionListener(){  
   		 public void actionPerformed(ActionEvent e) {              
   		     ClientGUI.emojii=1;
   		     popUpemji=new JPopupMenu();
   		     popUpemji.setBackground(new Color(0,0,0,0));
   		     popUpemji.setMaximumSize(new Dimension(504, 355));
   		     popUpemji.setMinimumSize(new Dimension(504, 355));
   		     popUpemji.setPreferredSize(new Dimension(504, 355));
   		     popUpemji.setLocation(700, 200);
   		     popUpemji.setVisible(true);
   		     popUpemji.add(panelemoji);
   		     popUpemji.show();
   		     JTextField txt=new JTextField();
   		 }  
   		}); 
        
        
		file.addActionListener(new ActionListener(){  
		    public void actionPerformed(ActionEvent e) {              
		       ClientGUI.filee=1;
		    }  
		   });  
		image.addActionListener(new ActionListener(){  
		    public void actionPerformed(ActionEvent e) {              
		       ClientGUI.imagee=1;
		    }  
		   });
		
		
		
		frame.setVisible(true);
	}
   public void sendMessageConnect() {
	   try { 
		   String url = "rmi://localhost:1099/akka";
	       Business server = (Business) Naming.lookup(url);  
	       BusnissClient busnissClient=new BusnissClient(FormLogin.nameUser, server);
	       busnissClient.messageConnect();
	   } 
	   catch (Exception e) { System.out.println("error: " + e); 
	   }
   }

   
   private void sendPic(Button icon) {
       panelImage.removeAll();
       for (int i = 1; i <=11; i++) {
           ButtonPanel buttonPanel = new ButtonPanel(icon.getName()+i+".png");
           buttonPanel.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
         	   emojii=1;
               }
           });
           panelImage.add(buttonPanel);
       }
       panelImage.revalidate();
       panelImage.repaint();
   }
   
}

