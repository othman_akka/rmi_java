package com.rmi.client.UI;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Button extends JButton{
	   private int index;
	
	
	    public Button(String image, int index) {
	        this.index = index;
	        setContentAreaFilled(false);
	        setBorder(null);
	        setPreferredSize(new Dimension(50, 32));
	        setCursor(new Cursor(Cursor.HAND_CURSOR));
	        setIcon(new ImageIcon(image));
	    }


		public int getIndex() {
			return index;
		}


		public void setIndex(int index) {
			this.index = index;
		}

}
