package com.rmi.client.UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.rmi.client.DAO.Implement.UsersDAOImplement;
import com.rmi.client.models.User;


public class FormLogin extends JFrame{

	JFrame frame;
	private JPanel panel;
	private JLabel lblLoginForm;
	private JLabel lblPassword;
	private JTextField textField;
	private JPasswordField passwordField;
	private JButton btnCancel;
	private JPanel panel_1;
	
	public static String nameUser="";
	
	public UsersDAOImplement bdd;
	public User user;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormLogin window = new FormLogin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("LoginClient");
		frame.setBounds(100, 100, 450, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(-12, 0, 446, 50);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblLoginForm = new JLabel("Login Form");
		lblLoginForm.setBounds(24, 11, 135, 29);
		lblLoginForm.setForeground(new Color(255, 255, 240));
		lblLoginForm.setFont(new Font("Tahoma", Font.BOLD, 24));
		panel.add(lblLoginForm);
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 51, 102));
		panel_1.setBounds(-2, 50, 436, 221);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Username :");
		lblNewLabel.setForeground(new Color(211, 211, 211));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(36, 36, 83, 26);
		panel_1.add(lblNewLabel);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setForeground(new Color(211, 211, 211));
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(36, 80, 83, 14);
		panel_1.add(lblPassword);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textField.setBounds(129, 41, 195, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		passwordField.setBounds(129, 79, 195, 20);
		panel_1.add(passwordField);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(30, 144, 255));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(235, 121, 89, 23);
		panel_1.add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bdd=new UsersDAOImplement();
				try {
					
					user=bdd.get(textField.getText().toString());
					
					if(user!=null) {
						
					     if(!textField.getText().toString().equals("admin") && passwordField.getText().toString().equals(user.getPassword()))
					      {
						    JOptionPane d = new JOptionPane();
						     d.showMessageDialog( frame, "connect as Client", "create an account",JOptionPane.PLAIN_MESSAGE);
						     setNameUser(textField.getText().toString());
						     System.out.println("ok");
						       new ClientGUI();
						   System.out.println("null");
						     frame.dispose();
						     textField.setText("");
						     passwordField.setText("");
					      }
					     else
					     {
					    	 JOptionPane d = new JOptionPane();
						     d.showMessageDialog( frame, "check from your password", "create account",JOptionPane.PLAIN_MESSAGE); 
					     }
					 }else {
						 JOptionPane d = new JOptionPane();
						    d.showMessageDialog( frame, "this account not exist", "create account",JOptionPane.PLAIN_MESSAGE);
						    textField.setText("");
						    passwordField.setText("");
					 }
					
				}catch (Exception e) 
				{
					e.getMessage();
				}	
			}
		});
		
		btnCancel = new JButton("Cancel");
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(255, 140, 0));
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCancel.setBounds(129, 121, 89, 23);
		panel_1.add(btnCancel);
		
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				
			}
		});
		
		JLabel lblNewLabel_1 = new JLabel("click here to create a new account");
		lblNewLabel_1.setForeground(new Color(240, 255, 240));
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
		         new RegisterForm();
				frame.dispose();
				
			}
			
		});
		lblNewLabel_1.setBounds(123, 157, 229, 20);
		panel_1.add(lblNewLabel_1);
	}
	public static String getNameUser() {
		return nameUser;
	}

	public static void setNameUser(String nameUser) {
		FormLogin.nameUser = nameUser;
	}
}

