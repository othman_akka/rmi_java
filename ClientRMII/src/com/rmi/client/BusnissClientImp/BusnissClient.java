package com.rmi.client.BusnissClientImp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTMLDocument;

import com.rmi.client.BusnissClientIn.Busniss;
import com.rmi.client.UI.ClientGUI;
import com.rmi.client.models.Message;
import com.rmi.server.Business.Business;

public class BusnissClient extends UnicastRemoteObject implements Busniss,Runnable {
    private final Business server;
    private String username = null;
    int cpt;
    static private String msg;
	public BusnissClient(String username, Business server) throws RemoteException {
	    this.username=username;
	    this.server=server;
	    server.user(this);
	}
	@Override
	public String getUserName() throws RemoteException {
		return this.username;
	}

	@Override
	public void sendMessage(String message) throws RemoteException {
		if(message.contains(".")) {
    	    String extension = message.substring(message.lastIndexOf("."));
    	    if(extension.equals(".jpg") || extension.equals(".png") || extension.equals(".jpeg"))
    	    {
    	    	Icon icon=new ImageIcon(message);
    	    	ClientGUI.textArea.insertIcon(icon);
    	    }
    	    else {
    	        try {
    	        	ClientGUI.textArea.getDocument().insertString(1,"\n"+message, null);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
    	    }
    	}else
    	{
    		 try {
 	        	ClientGUI.textArea.getDocument().insertString(1,"\n"+message, null);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
    	}
		
	}
    public void messageConnect() {
        String messageText = ClientGUI.textField_1.getText();
        setMsg(messageText);
            try {
                server.send(username + ": " + messageText);
                ClientGUI.textField_1.setText("");
                
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        
    }
  public void messageEmoji() {
    	   String emoji=ClientGUI.urlEmoji;
    	   System.out.println(ClientGUI.urlEmoji);
          setMsg(emoji);
            try {
                server.send(username + ": "+emoji);
                ClientGUI.textField_1.setText("");
                
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        
    }
    
    public void messageDisconnect() throws RemoteException {
            server.send(username + " déconnecté");
    }
    public void run() {
        while (true) {
        	System.out.println("");
        	  if (ClientGUI.index==1) {
                	   messageConnect();
                      ClientGUI.index =0;
              }
        	  else if( ClientGUI.emojii==1)
              {
            	  messageEmoji();
            	  ClientGUI.emojii=0;
              }else if(ClientGUI.filee==1)
              {
            	  
              }else if(ClientGUI.indexDisconnect==1)
              {
            	  try {
					messageDisconnect();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
              }
        }
    }
	@Override
	public String message() throws RemoteException {
		return getMsg();
	}
	public static String getMsg() {
		return msg;
	}
	public static void setMsg(String msg) {
		BusnissClient.msg = msg;
	}

    
}
