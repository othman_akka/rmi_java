package com.rmi.client.BusnissClientIn;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Busniss extends Remote{
	 public String getUserName() throws RemoteException;
	 public void sendMessage(String message) throws RemoteException; 
	 public String message()throws RemoteException;
}
