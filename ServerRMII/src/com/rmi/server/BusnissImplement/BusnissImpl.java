package com.rmi.server.BusnissImplement;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.rmi.client.BusnissClientIn.Busniss;
import com.rmi.server.Business.Business;
import com.rmi.server.DAO.Implement.UsersDAOImplement;
import com.rmi.server.models.MessageClient;

public class BusnissImpl extends UnicastRemoteObject implements Business {
    public static ArrayList<Busniss> array;
    public MessageClient client;
    public UsersDAOImplement bdd;
    public Busniss busniss;
    static private Busniss user;
	public BusnissImpl() throws RemoteException {
		super();
		array=new ArrayList<>();
		bdd=new UsersDAOImplement();
	}

	@Override
	public ArrayList getConnected() throws RemoteException {
		return array;
	}

	@Override
	public boolean user(Busniss user) throws RemoteException {
		     array.add(user);
		     BusnissImpl.user=user;
		     return true;
	}

	@Override
	public void send(String message) throws RemoteException
	{
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				 String date = simpleDateFormat.format(new Date());
			     client=new MessageClient(message, date);
				 try {
					bdd.addMessage(client);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		for(int i=0;i<array.size();i++)
		{
		    try{
		    	 
		    	busniss=(Busniss)array.get(i);
			    busniss.sendMessage(message);
		    }catch(Exception e){
		    	System.out.println("Erreur de userConnect:"+e.getMessage());
		    }
	    }
	}

}
