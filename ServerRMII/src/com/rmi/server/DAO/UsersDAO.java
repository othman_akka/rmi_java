package com.rmi.server.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import com.rmi.server.models.User;

public interface UsersDAO  {
	    boolean add(User user) throws SQLException;
	    boolean delete(String username)throws SQLException;
	    boolean update(User user)throws SQLException;
	    ArrayList<User> get()throws SQLException;
	    User get(String username)throws SQLException;
}
