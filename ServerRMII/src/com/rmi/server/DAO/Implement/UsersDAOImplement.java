package com.rmi.server.DAO.Implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.rmi.server.DAO.UsersDAO;
import com.rmi.server.DAO.Connection.ConnectionBD;
import com.rmi.server.models.MessageClient;
import com.rmi.server.models.User;
import com.rmi.server.models.UserEtat;

public class UsersDAOImplement implements UsersDAO {
	
	private Connection cnx=null;
	private Statement state=null;
	private ResultSet res;
	
	public UsersDAOImplement() {
		try {
			cnx=ConnectionBD.getConnection();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	@Override
	public boolean add(User user) throws SQLException {
		try {
			state=cnx.createStatement();
			state.executeUpdate("insert into login values('"+user.getUsername()+"','"+user.getPassword()+"','"+user.getName()+"','"+user.getAddress()+"')");
			return true;
		} catch (Exception e) {
			e.getMessage();
		}
		
		return false;
	}
	public boolean addMessage(MessageClient messageUser) throws SQLException {
		try {
			state=cnx.createStatement();
			state.executeUpdate("insert into message values('"+messageUser.getMessage()+"','"+messageUser.getDate()+"')");
			return true;
		} catch (Exception e) {
			e.getMessage();
		}
		
		return false;
	}

	@Override
	public boolean delete(String username) throws SQLException {
		try{
		cnx=ConnectionBD.getConnection();
		state=cnx.createStatement();
		state.executeUpdate("delete from login where username ='"+username+"'");
		return true;
		}catch (Exception e) {
			e.getMessage();
		}
		return false;
	}
	public ArrayList<User> getUsers() throws SQLException {
		ArrayList<User> arrayList=new ArrayList<>();
		try {
			state=cnx.createStatement();
			res=state.executeQuery("select * from login");
			while (res.next()) {
				User user=new User(res.getString("username"));
				arrayList.add(user);
			}
			return arrayList;
		} catch (Exception e) {
			e.getMessage();
		}
		
		return null;
	}
	public ArrayList<MessageClient> getMassages() throws SQLException {
		ArrayList<MessageClient> arrayList=new ArrayList<>();
		try {
			state=cnx.createStatement();
			res=state.executeQuery("select * from message");
			while (res.next()) {
				MessageClient user=new MessageClient(res.getString("username&message"),res.getString("date"));
				arrayList.add(user);
			}
			return arrayList;
		} catch (Exception e) {
			e.getMessage();
		}
		
		return null;
	}

	@Override
	public boolean update(User user) throws SQLException {
		try {
			state=cnx.createStatement();
			state.executeUpdate("update login set address ='"+user.getAddress()+"' where username='"+user.getUsername());
			return true;
		}catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	@Override
	public ArrayList<User> get() throws SQLException {
		ArrayList<User> listeUsers=new ArrayList<>();
		try {
			state=cnx.createStatement();
			res=state.executeQuery("select * from logion");
			while (res.next()) {
				listeUsers.add(new User(res.getString("username"),res.getString("password"),res.getString("name"),res.getString("address")));
			}
			return listeUsers;
			
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		
		return null;
	}

	@Override
	public User get(String username) throws SQLException {
		User user=null;
		try {
		
			state=cnx.createStatement();
			res=state.executeQuery("select * from login");
			while (res.next()) {
				if(res.getString("username").equals(username)) {
				user=new User(res.getString("username"),res.getString("password"),res.getString("name"),res.getString("address"));
				return user;
				}
			}
			
			
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return null;
	}

}
