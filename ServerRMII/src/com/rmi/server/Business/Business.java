package com.rmi.server.Business;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import com.rmi.client.BusnissClientIn.Busniss;

public interface Business extends Remote{
	 public ArrayList getConnected() throws RemoteException ;
	 public boolean user(Busniss a)throws RemoteException;
	 public void send(String s)throws RemoteException ;
}
