package com.rmi.server.UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.*;

import javax.swing.JTextField;

import com.rmi.server.DAO.Implement.UsersDAOImplement;
import com.rmi.server.models.User;
import com.toedter.calendar.JDateChooser;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class RegisterForm extends JFrame{

	public JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField_1;
	
	private User user;
	private UsersDAOImplement bdd;
	
	public RegisterForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(0, 0, 446, 50);
		
		JLabel label = new JLabel("Register");
		label.setForeground(new Color(255, 255, 240));
		label.setFont(new Font("Tahoma", Font.BOLD, 24));
		label.setBounds(26, 11, 135, 29);
		panel.add(label);
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(new Color(240, 248, 255));
		panel_1.setLayout(null);
		panel_1.setBackground(new Color(0, 51, 102));
		panel_1.setBounds(0, 49, 436, 352);
		
		JLabel label_1 = new JLabel("Username :");
		label_1.setForeground(new Color(211, 211, 211));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_1.setBounds(21, 167, 83, 23);
		panel_1.add(label_1);
		
		JLabel label_2 = new JLabel("Password :");
		label_2.setForeground(new Color(211, 211, 211));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_2.setBounds(21, 198, 83, 23);
		panel_1.add(label_2);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setColumns(10);
		textField.setBounds(129, 167, 195, 20);
		panel_1.add(textField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField.setBounds(129, 198, 195, 20);
		panel_1.add(passwordField);
		
		JButton button = new JButton("Create");
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button.setBackground(new Color(30, 144, 255));
		button.setBounds(235, 396, 89, 23);
		panel_1.add(button);
		
		JButton button_1 = new JButton("Cancel");
		button_1.setForeground(new Color(255, 255, 255));
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button_1.setBackground(new Color(255, 140, 0));
		button_1.setBounds(129, 396, 89, 23);
		panel_1.add(button_1);
		button_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
		     frame.dispose();
			}
		});
		
		JLabel label_3 = new JLabel("click here to Login");
		label_3.setForeground(new Color(240, 255, 240));
		label_3.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				 LoginForm form=new LoginForm();
		           form.frame.setVisible(true);
		           form.setBounds(100, 100, 450, 300);
		           form.pack();
		           form.setLocationRelativeTo(null);
				   form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				   frame.dispose();
			}
			
		});
		label_3.setBounds(210, 430, 126, 20);
		panel_1.add(label_3);
		
		
		JLabel lblFirst = new JLabel("First Name :");
		lblFirst.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFirst.setForeground(new Color(211, 211, 211));
		lblFirst.setBounds(21, 105, 83, 17);
		panel_1.add(lblFirst);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setForeground(new Color(211, 211, 211));
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLastName.setBounds(21, 133, 94, 23);
		panel_1.add(lblLastName);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_1.setBounds(129, 105, 195, 20);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_2.setBounds(129, 136, 195, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblRetypePass = new JLabel("Retype Pass");
		lblRetypePass.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRetypePass.setForeground(new Color(211, 211, 211));
		lblRetypePass.setBounds(21, 223, 83, 26);
		panel_1.add(lblRetypePass);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField_1.setBounds(129, 228, 195, 20);
		panel_1.add(passwordField_1);
		
		JLabel lblBirthdtae = new JLabel("BirthDtae :");
		lblBirthdtae.setForeground(new Color(211, 211, 211));
		lblBirthdtae.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblBirthdtae.setBounds(21, 260, 83, 30);
		panel_1.add(lblBirthdtae);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(129, 259, 195, 31);
		DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
		Date newDate = new Date();
		dateChooser.setDate(newDate);
		panel_1.add(dateChooser);
		frame.getContentPane().add(panel);//
		frame.getContentPane().add(panel_1);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setForeground(new Color(211, 211, 211));
		lblAddress.setBounds(21, 301, 83, 14);
		panel_1.add(lblAddress);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 15));
		textArea.setBounds(129, 301, 195, 69);
		panel_1.add(textArea);
		
	    button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(passwordField.getText().toString().equals(passwordField_1.getText().toString())) {
				    user=new User(textField.getText().toString(),passwordField.getText().toString() ,textField_1.getText().toString(),textArea.getText().toString());
					bdd=new UsersDAOImplement();
				try {
					if(bdd.add(user))
					{
						JOptionPane d = new JOptionPane();
						d.showMessageDialog( frame, "success", "create account",JOptionPane.PLAIN_MESSAGE);
					}
					else {
						JOptionPane d = new JOptionPane();
						d.showMessageDialog( frame, "Echec", "create account",JOptionPane.ERROR_MESSAGE);
					}
				}catch (Exception f) {
				    System.out.println("error in add user");
				}
			
				}else 
				{
					JOptionPane d = new JOptionPane();
					d.showMessageDialog( frame, "check your password","create account",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	    frame.setVisible(true);
	}
}
