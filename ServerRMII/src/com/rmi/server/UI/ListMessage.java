package com.rmi.server.UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.rmi.server.DAO.Implement.UsersDAOImplement;
import com.rmi.server.models.MessageClient;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ListMessage {
	
	JFrame frame;
	private JPanel panel;
	private JLabel lblLoginForm;
	private JPanel panel_1;
	private UsersDAOImplement bdd;
    private ArrayList<MessageClient>listMessage;
	private JLabel lblUsernameMessage;
	private JLabel lblDate;
	private JTextArea textArea;
	
	public ListMessage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("List of message");
		frame.setBounds(100, 100, 450, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(-12, 0, 446, 50);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblLoginForm = new JLabel("List of messages");
		lblLoginForm.setBounds(24, 11, 210, 29);
		lblLoginForm.setForeground(new Color(255, 255, 240));
		lblLoginForm.setFont(new Font("Tahoma", Font.BOLD, 24));
		panel.add(lblLoginForm);
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 51, 102));
		panel_1.setBounds(-2, 50, 436, 221);
		frame.getContentPane().add(panel_1);
		
		textArea = new JTextArea();
		textArea.setBounds(0, 0,302, 115);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(60, 51, 302, 115);
		bdd=new UsersDAOImplement();
		listMessage=new ArrayList<>();
		try {
			listMessage=bdd.getMassages();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for(int i=0;i<listMessage.size();i++)
		{
		  textArea.append(" " +listMessage.get(i).getMessage()+"       "+listMessage.get(i).getDate()+"\n");	
		}
		panel_1.setLayout(null);
		panel_1.add(scrollPane);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(textArea);
		
		lblUsernameMessage = new JLabel("Username & message");
		lblUsernameMessage.setForeground(new Color(255, 165, 0));
		lblUsernameMessage.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsernameMessage.setBounds(72, 11, 154, 27);
		panel_1.add(lblUsernameMessage);
		
		lblDate = new JLabel("Date");
		lblDate.setForeground(new Color(255, 165, 0));
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDate.setBounds(287, 17, 70, 14);
		panel_1.add(lblDate);
		
		JLabel lblNewLabel_1 = new JLabel("return to Home");
		lblNewLabel_1.setForeground(new Color(240, 255, 240));
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			   new ServerGUII();
				frame.dispose();
				
			}
			
		});
		lblNewLabel_1.setBounds(113, 178, 229, 20);
		panel_1.add(lblNewLabel_1);
		frame.setVisible(true);
		
	}
}
