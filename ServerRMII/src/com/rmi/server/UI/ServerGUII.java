package com.rmi.server.UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.rmi.server.BusnissImplement.BusnissImpl;
import com.rmi.server.DAO.Implement.UsersDAOImplement;
import com.rmi.server.models.User;
import com.rmi.server.models.UserEtat;

public class ServerGUII {

	JFrame frame;
	private JTextField textField;
	private JButton button;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JLabel label;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private LoginForm form;
	private JButton btnAjouter;
	private JButton btnModifier;
	private JButton btnSelectionner;
	ArrayList<User>  arrayList;
	UsersDAOImplement bdd;

	public ServerGUII()  {
		initialize();
		form=new LoginForm();
		 bdd=new UsersDAOImplement();
	}
	
		private void initialize() {
			frame = new JFrame("Server");
			frame.setBounds(100, 100, 450, 500);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			panel = new JPanel();
			panel.setForeground(Color.WHITE);
			panel.setBackground(new Color(0, 51, 102));
			panel.setBounds(10, 11, 414, 439);
			frame.getContentPane().add(panel);
			panel.setLayout(null);
			
			panel_1 = new JPanel();
			panel_1.setLayout(null);
			panel_1.setBackground(new Color(248, 148, 6));
			panel_1.setBounds(0, 0, 414, 38);
			panel.add(panel_1);
			
			textField = new JTextField();
			textField.setForeground(new Color(165, 42, 42));
			textField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textField.setEnabled(false);
			textField.setColumns(10);
			textField.setText(form.getNameUser());
			textField.setBounds(98, 11, 109, 22);
			panel_1.add(textField);
			
		    button = new JButton("Connect");
			button.setForeground(Color.GRAY);
			button.setFont(new Font("Tahoma", Font.PLAIN, 15));
			button.setBackground(new Color(0, 230, 64));
			button.setBounds(295, 11, 109, 22);
			panel_1.add(button);
			
	        button.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
				
					if(button.getText().toString().equals("Connect"))
					{
						button.setText("Disconnect");
						button.setBackground(new Color(236, 100, 75));
						  try {
					            java.rmi.registry.LocateRegistry.createRegistry(1099);
					            System.setSecurityManager(new RMISecurityManager());
					            Naming.rebind("rmi://localhost:1099/akka",new BusnissImpl());
					            }
					        catch (Exception f) {
					            f.printStackTrace();
					        }
						  
						 arrayList=new ArrayList<>();
					
						
						try {
							arrayList=bdd.getUsers();
							for(int i=0;i<arrayList.size();i++)
							{
								textArea.append(arrayList.get(i).getUsername()+ "  :Disconnected\n");
							}
							
						} catch (Exception e2) {
							
						}	
					}
					else
					{   
						button.setText("Disconnect");
						button.setBackground(new Color(236, 100, 75));
						 System.exit(0);
					}
				}
					
			});
			
			label = new JLabel("Your name :");
			label.setForeground(Color.WHITE);
			label.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label.setBounds(10, 11, 78, 22);
			panel_1.add(label);
			
		    panel_2 = new JPanel();
			panel_2.setBounds(10, 49,150, 330);
			panel.add(panel_2);
			panel_2.setLayout(null);
			
			
			textArea = new JTextArea();
			textArea.setBounds(3, 0,150, 330);
			textArea.setEditable(false);
			
			scrollPane = new JScrollPane();
			scrollPane.setBounds(3, 0,150,330);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setViewportView(textArea);;
			panel_2.add(scrollPane);
			
			JButton btnSupprimer = new JButton("Supprimer");
			btnSupprimer.setBackground(new Color(255, 140, 0));
			btnSupprimer.setFont(new Font("Times New Roman", Font.PLAIN, 14));
			btnSupprimer.setForeground(new Color(255, 255, 240));
			btnSupprimer.setBounds(278, 84, 113, 23);
			panel.add(btnSupprimer);
			
			btnSupprimer.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
				String username = JOptionPane.showInputDialog("Enter Your Name");
					try {
						if(bdd.delete(username))
						{
							  JOptionPane d = new JOptionPane();
							  d.showMessageDialog( frame, "Succes", "Delete client",JOptionPane.PLAIN_MESSAGE);
						}
						else {
							  JOptionPane d = new JOptionPane();
							  d.showMessageDialog( frame, "Echec", "Delete client",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (HeadlessException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			
			btnAjouter = new JButton("Ajouter");
			btnAjouter.setBackground(new Color(255, 140, 0));
			btnAjouter.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAjouter.setForeground(new Color(255, 255, 240));
			btnAjouter.setBounds(278, 160, 113, 23);
			panel.add(btnAjouter);
			btnAjouter.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					new RegisterForm();
					
				}
			});
			
			btnModifier = new JButton("Modifier");
			btnModifier.setBackground(new Color(255, 140, 0));
			btnModifier.setForeground(new Color(255, 255, 240));
			btnModifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnModifier.setBounds(278, 236, 113, 23);
			panel.add(btnModifier);
			
			btnSelectionner = new JButton("Selectionner");
			btnSelectionner.setBackground(new Color(255, 140, 0));
			btnSelectionner.setForeground(new Color(255, 255, 240));
			btnSelectionner.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnSelectionner.setBounds(278, 315, 113, 23);
			panel.add(btnSelectionner);
			
			JButton btnAfficherTousLes = new JButton("Afficher tous les messages");
			btnAfficherTousLes.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAfficherTousLes.setForeground(new Color(255, 255, 240));
			btnAfficherTousLes.setBackground(new Color(255, 165, 0));
			btnAfficherTousLes.setBounds(192, 394, 199, 23);
			panel.add(btnAfficherTousLes);
			btnAfficherTousLes.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					new  ListMessage();
				}
			});
			
			
			btnSelectionner.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					textArea.setText("");
					
					try {
						arrayList=bdd.getUsers();
						for(int j=0;j<arrayList.size();j++) {
					    if(BusnissImpl.array.size()!=0) {
						for(int i=0;i<BusnissImpl.array.size();i++) {
							if(arrayList.get(j).getUsername().equals(BusnissImpl.array.get(i).getUserName()))
						      textArea.append(BusnissImpl.array.get(i).getUserName()+"  :have connected \n");
							else
							{
							  textArea.append(arrayList.get(j).getUsername()+"  :Disconnected \n");
							}
						}
						}
					    else {
					    	 textArea.append(arrayList.get(j).getUsername()+"  :Disconnected \n");
					    }}
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (RemoteException e) {
						e.printStackTrace();
					}
					
				}
			});
			frame.setVisible(true);
			
		}
	}
