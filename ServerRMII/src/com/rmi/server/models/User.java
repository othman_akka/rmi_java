package com.rmi.server.models;

import java.io.Serializable;

public class User implements Serializable{
    private String username="";
    private String password="";
    private String name="";
    private String address="";
 
	public User()
	{
		
	}
	public User(String username)
	{
		 this.username = username;
	}
    public User(String username, String password) {
	        this.username = username;
	        this.password = password;
	 }
    
    public User(String username, String password, String name, String address) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.address = address;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
