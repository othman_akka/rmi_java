package com.rmi.server.models;

import java.io.Serializable;

public class MessageClient implements Serializable {
    private String message;
    private String date;
    
	public MessageClient() {
		super();
	}
	public MessageClient(String date) {
		super();
		this.date = date;
	}
	public MessageClient( String message, String date) {
		super();
		this.message = message;
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
    
}
