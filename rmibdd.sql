-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 27 Janvier 2019 à 23:53
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `rmibdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(500) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `login`
--

INSERT INTO `login` (`username`, `password`, `name`, `address`) VALUES
('admin', 'admin', 'admin', 'admin.admin@gmail.com'),
('', '', '', ''),
('aziz', 'aziz', 'aziz', 'aziz.aka@gmail.com'),
('hicham', 'akka', 'hicham', 'hicham.aka@gmail.com'),
('akka', 'akka', 'othman', 'othman.aka@gmail.com'),
('akka', 'akka', 'akka', 'akka.akka@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `username&message` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`username&message`, `date`) VALUES
('aziz: hi', '2019-57-27 01:57:03'),
('akka: hello', '2019-57-27 01:57:11'),
('akka: null', '2019-57-27 01:57:14'),
('akka: C:UserspceclipseClientRMIIemotions1.png', '2019-57-27 01:57:16'),
('akka: C:UserspceclipseClientRMIIemotions1.png', '2019-57-27 01:57:18'),
('aziz: hhhh', '2019-57-27 01:57:54'),
('akka: hi', '2019-07-27 02:07:31'),
('aziz: hello', '2019-07-27 02:07:49'),
('aziz: null', '2019-07-27 02:07:56'),
('aziz: C:UserspceclipseClientRMIIemotions4.png', '2019-08-27 02:08:00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(200) NOT NULL,
  `etat` int(200) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
